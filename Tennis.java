/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minitennis;

/**
 *
 * @author JLukunga
 */
public class Tennis {
    private final String firstPlayerName;
    private final String secondPlayerName;
    private int scorePlayer1;
    private int scorePlayer2;
    
    /**
     * Constructor
     * @param firstPlayerName
     * @param secondPlayerName
     */
    public Tennis(String firstPlayerName, String secondPlayerName){
        this.firstPlayerName = firstPlayerName;
        this.secondPlayerName = secondPlayerName;
        this.scorePlayer1 = 0;
        this.scorePlayer2 = 0;
    }
    
    /**
     * 
     * @return the score of the first player 
     */
    public int getScorePlayer1(){
        return this.scorePlayer1;
    }
    
    /**
     * 
     * @return the score of the second player
     */
    public int getScorePlayer2(){
        return this.scorePlayer2;
    }
    
    /**
     * 
     * @param score the new score of the player
     */
    public void setScorePlayer1(int score){
        this.scorePlayer1 = score;
    }
    
    /**
     * 
     * @param score the new score of the player
     * 
     */
    public void setScorePlayer2(int score){
        this.scorePlayer2 = score;
    }
    
    /**
     * 
     */
    public void firstPlayerScore(){
        this.scorePlayer1++;
    }
    
    /**
     * 
     */
    public void secondPlayerScore(){
        this.scorePlayer2++;
    }
    
    // Impletentation of rules
    
    /**
     * 
     * @return boolean value to design if game is in deuce or not
     */
    public boolean deuce(){
        return ( (this.scorePlayer1>=4) && (this.scorePlayer1 == this.scorePlayer2) );
    }
    
    /**
     * 
     * @return the boolean value designing if one of player has an advantage.
     * It's true if both have same score, difference of score is least than 2 and different of 0
     */
    public boolean advantage(){
        if( (this.scorePlayer1 >= 4 && this.scorePlayer2 >=4) &&
                (Math.abs(this.scorePlayer1 - this.scorePlayer2) == 1) )  {
            
            System.out.println("Adv");
            return true;
            
        }
        return false;
    }
    
    /**
     * 
     * @return boolean value describing if one of player has reached the conditions of winning
     */
    public boolean playerWon(){
        if( ((this.scorePlayer1 >= 4) || (this.scorePlayer2 >= 4)) && (Math.abs(this.scorePlayer1 - this.scorePlayer2) >= 2)){
            return true;
			}
        }
        return false;
    }
    
    /**
     * 
     * @return the name of the player with the higher score
     */
    public String playerWithHigherScore(){
        if(this.scorePlayer1 > this.scorePlayer2){
            return (this.firstPlayerName);
        }
        if(this.scorePlayer2 > this.scorePlayer1){
            return (this.secondPlayerName);
        }
        
        return "Equality";
    }
    
    /**
     * 
     * @param score
     * @return the String translation of the score or if error it 
     * constructs an IllegalArgumentException with the specified detail messages
     */
    public String convertScore(int score){
        switch(score){
            case 0: return "love";
            case 1: return "fifteen";
            case 2: return "thirty";
            case 3: return "forty";
        }
        throw new IllegalArgumentException("Illegal score : " + score);
    }
    
    /**
     * 
     * @return the state of the game depending of actual score of the player
     */
    public String manageGame(){
        if(this.deuce()){
            return ("Deuce");
        }
        if(this.advantage()){
            return ("Advantage : " + his.playerWithHigherScore());
        }
        if(this.playerWon()){
            return (this.playerWithHigherScore() + " wins");
        }
        return "";
    }
}
